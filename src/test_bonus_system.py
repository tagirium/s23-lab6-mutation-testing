from bonus_system import calculateBonuses
from random import choice

pr_mappings = {"Standard": 0.5, "Premium": 0.1, "Diamond": 0.2}
am_mappings = {
    9999: 1, 10000: 1.5, 10001: 1.5,
    49999: 1.5, 50000: 2, 50001: 2,
    99999: 2, 100000: 2.5, 100001: 2.5
}

def calculate_bonus(amount):
    if amount < 10000:
        bonus = 1
    elif amount < 50000:
        bonus = 1.5
    elif amount < 100000:
        bonus = 2
    else:
        bonus = 2.5
    return bonus
    
def test_invalid_program():
    for pr in 'abcdefghijklmnopqrstuvwxyz':
        for amount in am_mappings.keys():
            assert calculateBonuses(pr, amount) == 0

def test_valid_amount():
    for pr in pr_mappings.keys():
        for amount in am_mappings.keys():
            assert calculateBonuses(pr, amount) == am_mappings[amount]*pr_mappings[pr]
